import React, { Component } from 'react'
import Header from './Components/Header'
import Navbar from './Components/Navbar'
import Inbox from './Components/Pages/Inbox'
import Today from './Components/Pages/Today'
import Projects from './Components/Pages/Projects'
import Upcomming from './Components/Pages/Upcomming'
import ProjectTodos from './Components/Pages/ProjectTodos'
import { connect } from 'react-redux'
import { getTasks } from './Redux'
import { Route } from 'react-router-dom'
// const theme = createTheme({
//   palette: {
//     primary: {
//       main: purple[500],
//     },
//     secondary: {
//       main: green[500],
//     },
//   },
// });
class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    // this.props.getTasks()
    return (
      <>
        <Header />
        <Navbar />
        <Route path='/' exact  >
          <Inbox />
        </Route>
        <Route path='/Inbox'  >
          <Inbox />
        </Route>
        <Route path='/Today' >
          <Today />
        </Route>
        <Route path='/Upcomming' >
          <Upcomming />
        </Route>
        <Route path='/Projects' exact>
          <Projects />
        </Route>
        <Route path='/Projects/:projectId' component={ProjectTodos} ></Route>
      </>
    )
  }
}
function mapStateToProps(state) {
  return {
    state: state
  }
}
const mapDispatchToProps = {
  getTasks: getTasks
}

export default connect(mapStateToProps, mapDispatchToProps)(App)


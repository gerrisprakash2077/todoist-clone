import React, { Component } from 'react'
import Addtodo from '../Common/Addtodo'
import { connect } from 'react-redux'
import { getTasks, closeTask, deleteTask, editBox, editBoxRemove } from '../../Redux'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import './Today.css'
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import LinearProgress from '@mui/material/LinearProgress';
import {
    Button,
    Box,
    InputBase,
    Stack
} from '@mui/material'
function getTodaysDate() {
    const date = new Date()
    let day = date.getDate()
    if (Number(day) <= 9) {
        day = '0' + day
    }
    let month = date.getMonth() + 1
    if (Number(month) <= 9) {
        month = '0' + month
    }
    let year = date.getFullYear()
    let currentDate = `${year}-${month}-${day}`
    return currentDate
}
class ProjectTodos extends Component {
    constructor(props) {
        super(props)
        this.state = {
            addTodo: false,
            deleteModal: false,
            editTodo: false
        }
    }
    componentDidMount() {
        this.props.getTasks()

    }
    componentDidUpdate() {
        // this.props.getTasks()
    }
    deleteModal = () => {
        if (this.state.deleteModal) { }
    }
    close = (task) => {
        this.props.closeTask(task.id + '')
        console.log(task.id);
    }
    delete = (id) => {
        this.props.deleteTask(id + '')
        console.log(id);
    }
    edit = (task) => {
        if (!this.state.editTodo) {
            this.setState({
                editTodo: true
            })
            this.props.editBox(task.id, task.content, task.description, task.date)
        } else {
            this.setState({
                editTodo: false
            })
            // this.props.editBoxRemove(task.id, task.content, task.description, task.date)
        }
    }
    dataChecker = () => {
        if (this.props.state.tasks) {
            let data = this.props.state.tasks.filter((tasks) => {
                if (tasks.projectId == this.props.match.params.projectId) {
                    return true
                }
            })
            if (!data.length) {
                return <img className='ml-auto mr-auto mb-20 w-96' src="https://play-lh.googleusercontent.com/OtYGjL0DfKW7NjLNOaxN3-y8oUZpeiKdYYI1QPg-62NRLri2uVCDh_JW-8aP6vI3ww" alt="" />
            }
            return data.map((tasks) => {
                if (tasks.editBox && this.state.editTodo) {
                    console.log('fg');
                    return <div key='edit-box'>
                        <Addtodo content={tasks.content} description={tasks.description} date={getTodaysDate()} cancelButton={() => {
                            this.edit(tasks)
                        }} id={tasks.id} todayState={this.state} submit={'SAVE'} />
                    </div>
                } else {
                    return <div key={tasks.id}>
                        <div className="flex  my-5">
                            <div className='mr-2'>
                                <InputBase onChange={() => {
                                    this.close(tasks)
                                }} type="radio" className='cursor-pointer' />
                            </div>
                            <div className='mr-20'>
                                <h2 >{tasks.content}</h2>
                                <p className='text-slate-400 text-sm my-2'>{tasks.description}</p>
                            </div>
                            <ModeEditIcon className='absolute right-10 cursor-pointer' onClick={() => {
                                this.edit(tasks)
                            }} />
                            <DeleteForeverIcon onClick={() => {
                                this.delete(tasks.id)
                            }} className='absolute right-0 cursor-pointer' />
                        </div>
                        <hr />
                    </div>
                }
            })

        } else {
            return <div className=' bg-red'>
                {/* <img src="https://media.tenor.com/V2XxMOl6dMMAAAAM/ashtonjones-naruto.gif" alt="" className='w-full' /> */}
                <LinearProgress color='warning' />
            </div>
        }
    }
    addTodoTogle = () => {
        if (!this.state.addTodo) {
            this.setState({
                ...this.state,
                addTodo: true
            })
        } else {
            this.setState({
                ...this.state,
                addTodo: false
            })
        }
    }
    editTodoTogle = () => {
        if (!this.state.editTodo) {
            this.setState({
                editTodo: true
            })
        } else {
            this.setState({
                editTodo: false
            })
        }
    }
    addTodo = () => {
        if (this.state.addTodo) {
            return <Addtodo content={''} description={''} date={''} todayState={this.state} projectId={this.props.match.params.projectId} cancelButton={this.addTodoTogle} submit={'SUBMIT'} />
        } else {
            return <Button color='warning' onClick={this.addTodoTogle}>+add todo</Button>
        }
    }
    render() {

        return (
            <div className={this.props.state.navbar ? 'ml-72 overflow-y-scroll ' : ''} id='today'>
                <div className='flex flex-col items-center'>
                    <h2 className='my-8 text-3xl'>{this.props.state.currentProject}</h2>
                    <div id='today-items' className='relative today-items'>{this.dataChecker()}</div>
                    {this.addTodo()}
                </div>
                {this.deleteModal()}
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    getTasks: getTasks,
    closeTask: closeTask,
    deleteTask: deleteTask,
    editBox: editBox,
    editBoxRemove: editBoxRemove
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectTodos)

import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    getProjects,
    addProjecttogle,
    setCurrentProject,
    deleteProject
} from '../../Redux'
import {
    Button
} from '@mui/material'
import LinearProgress from '@mui/material/LinearProgress';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import AddProjectmodal from '../Common/AddProjectmodal';
import { NavLink } from 'react-router-dom';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import AddLinkIcon from '@mui/icons-material/AddLink';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { CopyToClipboard } from 'react-copy-to-clipboard'
class Projects extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dropdown: {}
        }
    }

    componentDidMount() {
        this.props.getProjects()
    }
    dataChecker = () => {
        if (this.props.state.projects) {
            return this.props.state.projects.map((project) => {
                if (project.name == 'Inbox') {
                    return
                }
                return <div key={project.id} className='my-2 py-2 px-4 hover:bg-slate-200 w-full rounded-lg'>
                    <AddProjectmodal name={project.name} />
                    <div className='flex  justify-between'>
                        <NavLink to={`/Projects/${project.id}`} onClick={() => {
                            this.props.setCurrentProject(project.name);
                        }}>
                            <div className='flex'>
                                <FiberManualRecordIcon fontSize='5px text-gray-400' className='mr-3 mt-auto mb-auto' />
                                <h3 >{project.name}</h3>
                            </div>
                        </NavLink>
                        <div className='relative'>
                            <MoreVertIcon className='cursor-pointer' onClick={() => {
                                console.log('clicked');
                                if (this.state.dropdown[project.id]) {
                                    let obj = {}
                                    obj[project.id] = false
                                    this.setState({
                                        dropdown: {
                                            ...obj
                                        }
                                    })
                                } else {
                                    let object = {}
                                    object[project.id] = true
                                    this.setState({
                                        dropdown: {
                                            ...object
                                        }
                                    })
                                }
                            }} />
                            {this.state.dropdown[project.id] ? <div className='absolute bg-gray-100 p-3 right-4 w-56'>
                                <div>
                                    <Button onClick={() => {
                                        this.props.addProjecttogle(project.name)
                                        // this.props.setCurrentProject(project.id);
                                    }}><div className='text-black'><BorderColorIcon className='mr-5' />Edit project</div></Button>
                                </div>
                                <div>
                                    <Button onClick={() => {
                                        this.props.deleteProject(project.id)
                                    }}><div className='text-black'><DeleteOutlineIcon className='mr-5' />Delete project</div></Button>
                                </div>
                                <div>
                                    <CopyToClipboard>
                                        <Button><div className='text-black'><AddLinkIcon className='mr-5' />Copy link</div></Button>
                                    </CopyToClipboard>
                                </div>
                            </div> : null}
                        </div>
                    </div>
                </div>

            })
        } else {
            return <LinearProgress color='warning' />
        }
    }
    render() {
        return (
            <div className={this.props.state.navbar ? 'ml-72 overflow-y-scroll ' : ''} id='today'>
                <div className='flex flex-col items-center'>
                    <h2 className='my-8 text-3xl'>Projects</h2>
                    <div id='today-items' className={this.props.state.projects ? 'relative today-items list-disc flex flex-col items-start' : 'relative today-items list-disc'}>
                        <h2 className='ml-auto mr-5 my-10'><Button color='warning' onClick={() => { this.props.addProjecttogle(' ') }}>+ Add Project</Button></h2>
                        {this.dataChecker()}
                    </div>
                </div>

            </div >
        )
    }
}
function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    getProjects: getProjects,
    addProjecttogle: addProjecttogle,
    setCurrentProject: setCurrentProject,
    deleteProject: deleteProject
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects)

import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    addProjecttogle,
    addProject
} from '../../Redux'
import {
    Modal, InputBase, Stack, InputLabel, Box, Button
} from '@mui/material'
import './AddProjectmodal.css'
let c = 0;
class EditProject extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            color: ''
        }
    }
    name = (event) => {
        this.setState({
            name: event.target.value
        })
    }
    color = (event) => {
        this.setState({
            color: event.target.value
        })
    }
    componentDidUpdate() {
        if (this.state.name != this.props.state.addProjectModal && c++ == 0) {
            this.setState({
                name: this.props.state.addProjectModal
            })
        }
    }
    render() {
        return (
            <Modal open={Boolean(this.props.state.addProjectModal)} className='flex justify-center my-40'>
                <Box className='bg-white flex flex-col w-96 p-10 rounded-2xl '>
                    <h2>Add Project</h2>
                    <div>
                        <label htmlFor="name">Name</label>
                        <input type="text" id='name' value={this.state.name} onChange={this.name} />

                    </div>
                    <div>
                        <label htmlFor="color" className='mr-4'>Color</label>
                        <input type="color" id='color' className='w-5 h-5' value={this.state.color} onChange={this.color} />
                    </div>
                    <div>

                        <Button color='warning' onClick={this.props.addProjecttogle}>Cancel</Button>
                        <Button color='warning' variant='contained' onClick={() => {
                            this.props.addProject({
                                name: this.state.name
                            })
                            this.props.addProjecttogle()

                        }
                        }>Add</Button>
                    </div>

                </Box>

            </Modal>
        )
    }
}
function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    addProjecttogle: addProjecttogle,
    addProject: addProject
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProject)







import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    addProjecttogle,
    addProject,
    editProject
} from '../../Redux'
import {
    Modal, InputBase, Stack, InputLabel, Box, Button
} from '@mui/material'
import './AddProjectmodal.css'
let c = 0;
class AddProjectmodal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            color: ''
        }
    }
    name = (event) => {
        this.setState({
            name: event.target.value
        })
    }
    color = (event) => {
        this.setState({
            color: event.target.value
        })
    }
    componentDidUpdate() {
        if (this.state.name != this.props.state.addProjectModal && c++ == 0) {
            this.setState({
                name: this.props.state.addProjectModal
            })
        }
    }
    render() {
        return (
            <Modal open={Boolean(this.props.state.addProjectModal)} className='flex justify-center my-96'>
                <Box className='bg-white flex flex-col w-1/3 pt-10 px-10 pb-0 rounded-2xl '>
                    <h2 className='text-center text-xl'>Add Project</h2>
                    <div>
                        <label htmlFor="name" className='mr-3'>Name</label>
                        <input type="text" placeholder='Enter project Name' id='name' value={this.state.name} className='p-3 my-1' onChange={this.name} />

                    </div>
                    {/* <div>
                        <label htmlFor="color" className='mr-4'>Color</label>
                        <input type="color" id='color' className='w-5 h-5' value={this.state.color} onChange={this.color} />
                    </div> */}
                    <div>


                        <Button color='warning' className='float-right' variant='contained' onClick={() => {
                            if (this.props.state.addProjectModal == '') {
                                this.props.addProject({
                                    name: this.state.name
                                })
                                this.props.addProjecttogle()
                            } else {
                                this.props.addProjecttogle()
                            }

                        }
                        }>Add</Button>
                        <Button color='warning' className='float-right' onClick={this.props.addProjecttogle}>Cancel</Button>
                    </div>

                </Box>

            </Modal>
        )
    }
}
function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    addProjecttogle: addProjecttogle,
    addProject: addProject,
    editProject: editProject
}

export default connect(mapStateToProps, mapDispatchToProps)(AddProjectmodal)







import React, { Component } from 'react'
import { List, Box, ListItem, ListItemButton, Button } from '@mui/material'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import TodayIcon from '@mui/icons-material/Today';
import InboxIcon from '@mui/icons-material/Inbox';
import { NavLink } from 'react-router-dom';
import './Navbar.css'
import { connect } from 'react-redux'
import { addProject, getProjects, setCurrentProject } from '../Redux'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import CircularProgress from '@mui/material/CircularProgress';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dropDown: false
        }
    }
    displayProjects = () => {
        if (this.props.state.projects) {
            return this.props.state.projects.map((project) => {
                if (project.name == 'Inbox') {
                    return
                }
                return <NavLink to={`/Projects/${project.id}`} key={project.id} onClick={() => {
                    this.props.setCurrentProject(project.name);
                }}>
                    <ListItem  >
                        <ListItemButton className='text-sm' ><FiberManualRecordIcon fontSize='3px' className='mr-2' />{project.name}</ListItemButton>
                    </ListItem>
                </NavLink>
            })
        } else {
            return <CircularProgress color='warning' />
        }
    }
    render() {
        return (
            <div className={this.props.state.navbar ? 'float-left navbar' : 'none'}  >
                <Box sx={{
                    height: '93.2vh'
                }} className='bg-slate-100 w-72 '>
                    <List >
                        <NavLink to='/Inbox'>
                            <ListItem  >
                                <ListItemButton ><InboxIcon className='mr-2' />Inbox</ListItemButton>
                            </ListItem>
                        </NavLink>

                        <NavLink to='/Today'>
                            <ListItem  >
                                <ListItemButton ><TodayIcon className='mr-2' />Today</ListItemButton>
                            </ListItem>
                        </NavLink>

                        <NavLink to='/Upcomming'>
                            <ListItem  >
                                <ListItemButton ><CalendarMonthIcon className='mr-2' />Upcomming</ListItemButton>
                            </ListItem>
                        </NavLink>

                        <ListItem  >
                            <NavLink to='/Projects'>
                                <ListItemButton >Projects</ListItemButton>
                            </NavLink>
                            <Button color='warning' onClick={() => {
                                this.props.getProjects()
                                if (this.state.dropDown) {
                                    this.setState({
                                        dropDown: false
                                    })
                                } else {
                                    this.setState({
                                        dropDown: true
                                    })
                                }
                            }} ><ExpandMoreIcon /></Button>
                        </ListItem >
                        {this.state.dropDown ?
                            this.displayProjects()
                            : ''}


                    </List>
                </Box>
            </div >
        )
    }
}

function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    addProject: addProject,
    getProjects: getProjects,
    setCurrentProject: setCurrentProject

}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)

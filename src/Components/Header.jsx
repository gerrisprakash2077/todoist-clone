import React, { Component } from 'react'
import {
    AppBar,
    Typography,
    Toolbar,
    TextField,
    Button
} from '@mui/material'
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import { connect } from 'react-redux'
import { navbarTogler } from '../Redux'
import { NavLink } from 'react-router-dom';
import MenuSharpIcon from '@mui/icons-material/MenuSharp';
class Header extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className='h-16'>
                <AppBar className='flex'>
                    <Toolbar className='bg-red-500'>
                        <Typography className='bg-red-500'>
                            <Button >
                                <MenuSharpIcon onClick={this.props.navbarTogler} sx={{ fill: 'white' }} />
                            </Button>
                            <NavLink to='/Today'>
                                <Button >
                                    <HomeOutlinedIcon sx={{ fill: 'white' }} />
                                </Button>
                            </NavLink>

                        </Typography>
                        <TextField
                            color='warning'
                            className='bg-red-200 rounded-xl'
                            size='small'
                            placeholder='Search'
                        >

                        </TextField>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    navbarTogler: navbarTogler
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)

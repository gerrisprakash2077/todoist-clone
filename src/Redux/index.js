import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk'
import { TodoistApi } from "@doist/todoist-api-typescript"
const api = new TodoistApi("18d8628af565360cae2ecf40b3d64c4519a1e4a9")
// let id=0;
function getTasksAction(data) {
    return {
        type: 'GET_TASKS_ACTION',
        payload: data
    }
}
function getProjectsAction(projects) {
    return {
        type: 'GET_PROJECTS_ACTION',
        payload: projects
    }
}
function addTaskAction(data) {
    return {
        type: 'ADD_TASKS_ACTION',
        payload: data
    }
}
function closeTaskAction(id) {
    return {
        type: 'CLOSE_TASK_ACTION',
        payload: id
    }
}
function deleteTaskAction(id) {
    return {
        type: 'DELETE_TASK_ACTION',
        payload: id
    }
}
function updateTaskAction(data) {
    return {
        type: 'UPDATE_TASK_ACTION',
        payload: data
    }
}
export function editBox(id, content, description, date) {
    return {
        type: 'EDIT_BOX',
        payload: {
            id: id,
            content: content,
            description: description,
            date: date
        }

    }
}
export function editBoxRemove(id, content, description, date) {
    return {
        type: 'EDIT_BOX_REMOVE',
        payload: {
            id: id,
            content: content,
            description: description,
            date: date
        }

    }
}
export function navbarTogler() {
    return {
        type: 'NAVBAR_TOGLER'
    }
}
export function addProjecttogle(id) {
    return {
        type: 'ADD_PROJECT_TOGLE',
        payload: id
    }
}

function reducer(state, action) {
    switch (action.type) {
        case 'GET_TASKS_ACTION':
            // console.log(action.payload);
            if (!action.payload) {
                return {
                    ...state,
                    tasks: null
                }
            }
            return {
                ...state,
                tasks: [...action.payload]

            }
        case 'ADD_TASKS_ACTION':
            return {
                ...state,
                tasks: [...state.tasks, action.payload]
            }
        case 'CLOSE_TASK_ACTION':
            let closeTaskData = state.tasks.filter((task) => {
                return task.id != action.payload
            })
            return {
                ...state,
                tasks: [...closeTaskData]
            }
        case 'DELETE_TASK_ACTION':
            let deleteTaskData = state.tasks.filter((task) => {
                return task.id != action.payload
            })
            return {
                ...state,
                tasks: [...deleteTaskData]
            }
        case 'UPDATE_TASK_ACTION':
            let updateTaskData = state.task.map((task) => {
                if (task.id == action.payload.data.id) {
                    return { ...action.payload }
                } else {
                    return task
                }
            })
            return {
                ...state,
                tasks: [...updateTaskData]
            }
        case 'EDIT_BOX':
            let editBoxData = state.tasks.map((task) => {
                if (task.id == action.payload.id) {
                    return {
                        ...task,
                        editBox: {
                            ...action.payload
                        }
                    }
                } else {
                    delete task.editBox
                    return task
                }
            })
            return {
                ...state,
                tasks: [...editBoxData]
            }
        case 'EDIT_BOX_REMOVE':
            let editBoxRemoveData = state.tasks.map((task) => {
                if (task.id != action.payload.id) {
                    delete task.editBox
                    return {
                        ...task,
                    }
                } else {
                    delete task.editBox
                    return task
                }
            })
            return {
                ...state,
                tasks: [...editBoxRemoveData]
            }
        case 'NAVBAR_TOGLER':
            if (state.navbar) {
                return {
                    ...state,
                    navbar: false
                }
            } else {
                return {
                    ...state,
                    navbar: true
                }
            }
        case 'GET_PROJECTS_ACTION':
            if (!action.payload) {
                return {
                    ...state,
                    projects: null
                }
            }
            return {
                ...state,
                projects: [...action.payload]

            }
        case 'ADD_PROJECT_TOGLE':
            if (state.addProjectModal == '') {
                console.log(action.payload);
                return {
                    ...state,
                    addProjectModal: action.payload
                }
            } else {
                return {
                    ...state,
                    addProjectModal: ''
                }
            }

        // } else {
        //     return {
        //         ...state,
        //         addProjectModal: false
        //     }
        // }
        case 'SET_CURRENT_PROJECT':
            return {
                ...state,
                currentProject: action.payload
            }
        default:
            return state

    }
}
export function getTasks() {
    return function (dispatch) {
        api.getTasks()
            .then((tasks) => {
                // console.log(tasks)
                dispatch(getTasksAction(tasks))
            })
            .catch((error) => {
                console.log(error)
            })
    }

}
export function addTask(content, description, date, projectId) {
    console.log(projectId + 'prrr');
    // console.log(content, description, date + '***');
    return function (dispatch) {
        let data = {
            id: 1,
            content: content,
            description: description,
            date: date
        }
        // dispatch(addTaskAction(data))
        dispatch(getTasksAction(null))
        api.addTask({
            content: content,
            projectId: projectId
        })
            .then((task) => {
                console.log(task)
                api.updateTask(task.id, {
                    due_date: date,
                    description: description
                })
                    .then((isSuccess) => {
                        console.log(isSuccess)
                        // dispatch(addTaskAction(isSuccess))
                        dispatch(getTasks())
                        //getTasks()
                    })
                    .catch((error) => {
                        console.log(error)
                    })
            })
            .catch((error) => {
                console.log(error)
            })

    }


}
export function closeTask(id) {
    return function (dispatch) {
        dispatch(getTasksAction(null))
        api.closeTask(id)
            .then((isSuccess) => {
                console.log(isSuccess)
                // dispatch(closeTaskAction(id))
                dispatch(getTasks())
            })
            .catch((error) => {
                console.log(error)
            })
    }
}
export function deleteTask(id) {
    return function (dispatch) {
        dispatch(getTasksAction(null))
        api.deleteTask(id)
            .then((isSuccess) => {
                console.log(isSuccess)
                // dispatch(deleteTaskAction(id))
                dispatch(getTasks())
            })
            .catch((error) => {
                console.log(error)
            })
    }
}
export function updateTask(id, content, description, date) {
    return function (dispatch) {
        dispatch(getTasksAction(null))
        api.updateTask(id, {
            content: content,
            due_date: date,
            description: description
        })
            .then((isSuccess) => {
                console.log(isSuccess + 'up')
                dispatch(getTasks())
                // dispatch(updateTaskAction(isSuccess))
            })
            .catch((error) => {
                console.log(error)
            })
    }
}
export function addProject(data) {
    return function (dispatch) {
        dispatch(getProjectsAction(null))
        api.addProject({
            name: data.name,
        })
            .then((project) => {
                console.log(project + 'project success')
                dispatch(getProjects())
            })
            .catch((error) => {
                console.log(error)
            })
    }
}
export function getProjects() {
    return function (dispatch) {
        api.getProjects()
            .then((projects) => {
                dispatch(getProjectsAction(projects))
                // console.log(projects)
            })
            .catch((error) => {
                console.log(error)
            })
    }
}
export function setCurrentProject(name) {
    return {
        type: 'SET_CURRENT_PROJECT',
        payload: name
    }
}
export function deleteProject(id) {
    console.log('hehehhe');
    return function (dispatch) {
        dispatch(getProjectsAction(null))
        api.deleteProject(id)
            .then((isSuccess) => {
                console.log(isSuccess)
                dispatch(getProjects())
            })
            .catch((error) => {
                console.log(error)
            })
    }
}
export function editProject(id, name) {

    return function (dispatch) {
        dispatch(getProjectsAction(null))
        api.updateProject(id, { name: name })
            .then((isSuccess) => {
                console.log(isSuccess)
                dispatch(getProjects())
            })
            .catch((error) => {
                console.log(error)
            })
    }
}



const store = createStore(reducer, {
    projects: null,
    tasks: null,
    navbar: true,
    addProjectModal: '',
    currentProject: null
}, applyMiddleware(thunk))

store.subscribe(() => {
    console.log(store.getState());
})


export default store
